# No. 1
Repository : https://gitlab.com/frinaldisyauqi/image-classification

# No.2
### Image Classification Web App
Aplikasi berbasis web yang dapat memprediksi objek apa yang ada pada gambar tersebut 
<br/>
Link YouTube : https://youtu.be/r1OJUsQUMPI
<br/>
![demo-image](https://gitlab.com/frinaldisyauqi/image-classification/-/raw/main/docs/demo.gif)

# No.3

```mermaid
  graph TD
    A(fa:fa-image Upload Images) --> B
    B(Predict Image) --> C
    C(Print Out Results)
```

Menggunakan Convolutional Neural Network dari library tensorflow dengan kegunaan untuk membuat melatih model image classification dengan dataset yang tersedia
